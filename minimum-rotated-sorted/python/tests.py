import unittest, solution, random

class TestSolution(unittest.TestCase):

  def randlist(self, length):
    low = -1000
    up = 1000
    return [random.randint(low, up) for i in range(length)]

  def test_shortlists(self):
    cases = [self.randlist(i) for i in range(2, 1000)]
    for nums in cases:
      self.assertEqual(solution.findMin(nums), min(nums))

if __name__ == "__main__":
  unittest.main()