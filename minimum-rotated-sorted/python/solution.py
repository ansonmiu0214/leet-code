#!/bin/python3

"""
D&C approach: 
  O(log(n)) divide + O(1) conquer
= O(log(n)) running time
"""
def findMin(nums):
  count = len(nums)
  # D&C base case
  if count == 1:
    return nums[0]
  
  mid = count // 2

  # Divide
  left_min = findMin(nums[:mid])
  right_min = findMin(nums[mid:])

  # Conquer
  return min(left_min, right_min)

if __name__ == "__main__":
  arr = list(map(int, input().strip().split(' ')))
  minimum = findMin(arr)
  print(minimum)